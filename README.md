# README
## This is the Coal Graal Theme for VS Code.

![Coal Graal Theme](https://camo.githubusercontent.com/4db2614023d148fea77a6642abdb8e9c17ebfa9b/687474703a2f2f662e636c2e6c792f6974656d732f32453363327a324e336632333372336e306232372f636f616c677261616c2e706e67 "Coal Graal Theme")

### For more information
* [Coal Graal Github Page](https://github.com/baskerville/Coal-Graal.tmTheme)

**Enjoy!**